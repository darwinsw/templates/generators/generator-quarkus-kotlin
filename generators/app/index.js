import Generator from 'yeoman-generator'
import yosay from 'yosay'
import chalk from 'chalk'
import path from 'path'
import fs from 'fs'

export default class QuarkusKotlinGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts)
    this.argument('appname', {type: String, required: false})
  }

  prompting() {
    this.log(yosay(
      'Welcome to the first ' + chalk.red('generator-quarkus-hexagonal') + ' generator!'
    ))

    const prompts = [{
      type: 'input',
      name: 'groupId',
      message: 'Your project group id',
      default: 'com.example'
    }, {
      type: 'input',
      name: 'projectName',
      message: 'Your project name',
      default: this.options.appname || path.basename(process.cwd())
    }, {
      type: 'input',
      name: 'description',
      message: 'Your project description',
      default: this.options.appname || path.basename(process.cwd())
    }]

    return this.prompt(prompts).then(props => {
      this.props = props
    })
  }
  default() {
    if (path.basename(this.destinationPath()) !== this.props.projectName) {
      this.log(
        'Your project must be inside a folder named ' + this.props.projectName + '\n' +
        'I\'ll automatically create this folder.'
      )
      fs.mkdir(this.props.projectName, err => {
        if (err) {
          console.log(err)
        } else {
          console.log(`New directory '${this.props.projectName}' successfully created.`)
        }
      })
      this.destinationRoot(this.destinationPath(this.props.projectName))
    }
  }

  writing() {
    this.config.set(this.props)
    this.config.save()
  	const pkg = this.props.groupId.replace(/[.]/g, '/').replace(/-/g, '_')
    const prj = this.props.projectName.replace(/[.]/g, '/').replace(/-/g, '_')
  	const mainSrc = `src/main/kotlin/${pkg}`
    const mainResources = `src/main/resources`
  	const testSrc = `src/test/kotlin/${pkg}`
    const filesToCopy = [
      'README.md',
      'build.gradle',
      'settings.gradle',
      'gradle.properties',
      'gradlew',
      'gradlew.bat',
      'gradle/wrapper/gradle-wrapper.properties'
    ]

    const baseModuleName = this.props.groupId + '.' + this.props.projectName
    const basePackageName = baseModuleName.replace(/-/g, '_')

    const parameters = {
      ...this.props,
      baseModuleName,
      basePackageName
    }

    for(let file of filesToCopy) {
      this.fs.copyTpl(this.templatePath(file), this.destinationPath(file), parameters)
    }

    this.fs.copyTpl(this.templatePath('src/domain/build.gradle'),
      this.destinationPath(`${baseModuleName}.domain/build.gradle`), parameters)   
    this.fs.copyTpl(this.templatePath('src/domain/main/Hello.kt'),
      this.destinationPath(`${baseModuleName}.domain/${mainSrc}/${prj}/domain/Hello.kt`), parameters)
    this.fs.copyTpl(this.templatePath('src/domain/test/HelloTest.kt'),
      this.destinationPath(`${baseModuleName}.domain/${testSrc}/${prj}/domain/HelloTest.kt`), parameters)
    
    this.fs.copyTpl(this.templatePath('src/api/build.gradle'),
      this.destinationPath(`${baseModuleName}.api/build.gradle`), parameters)
    this.fs.copyTpl(this.templatePath('src/api/main/HelloResource.kt'),
      this.destinationPath(`${baseModuleName}.api/${mainSrc}/${prj}/api/HelloResource.kt`), parameters)
    this.fs.copyTpl(this.templatePath('src/api/resources/beans.xml'),
      this.destinationPath(`${baseModuleName}.api/${mainResources}/META-INF/beans.xml`), parameters)

    this.fs.copyTpl(this.templatePath('src/boot/build.gradle'),
      this.destinationPath(`${baseModuleName}.boot/build.gradle`), parameters)
    this.fs.copyTpl(this.templatePath('src/boot/main/BeanProducer.kt'),
      this.destinationPath(`${baseModuleName}.boot/${mainSrc}/${prj}/boot/BeanProducer.kt`), parameters)
    this.fs.copyTpl(this.templatePath('src/boot/resources/application.properties'),
      this.destinationPath(`${baseModuleName}.boot/${mainResources}/application.properties`), parameters)
    this.fs.copyTpl(this.templatePath('src/boot/resources/beans.xml'),
      this.destinationPath(`${baseModuleName}.boot/${mainResources}/META-INF/beans.xml`), parameters)
  }
}
