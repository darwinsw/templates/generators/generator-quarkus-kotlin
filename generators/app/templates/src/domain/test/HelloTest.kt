package <%= basePackageName %>.domain;

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class HelloTest {
	@Test
	fun shouldSayHelloWorld() {
		val sut = Hello();
		assertThat(sut.sayHello(), `is`(equalTo("Hello, <%= projectName %>!")))
	}
}