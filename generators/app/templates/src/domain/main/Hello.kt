package <%= basePackageName %>.domain

class Hello {
	fun sayHello() : String = "Hello, <%= projectName %>!"
}