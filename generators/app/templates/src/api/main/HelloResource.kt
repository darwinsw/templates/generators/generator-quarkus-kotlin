package <%= basePackageName %>.api

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import <%= basePackageName %>.domain.Hello
import java.time.ZonedDateTime

@Path("/hello")
class HelloResource(val hello: Hello) {
    @GET()
    @Path("sayHello")
    @Produces(MediaType.APPLICATION_JSON)
    fun hello() = HelloResponse(hello.sayHello(), ZonedDateTime.now())
}

data class HelloResponse(val text: String, val now: ZonedDateTime)
