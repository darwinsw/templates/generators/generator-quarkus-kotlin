package <%= basePackageName %>.boot

import <%= basePackageName %>.domain.Hello
import javax.enterprise.context.ApplicationScoped

class BeanProducer {
    @ApplicationScoped
    fun messageSource(): Hello {
        return Hello()
    }
}
