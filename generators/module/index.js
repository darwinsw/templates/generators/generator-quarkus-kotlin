import Generator from 'yeoman-generator'
import yosay from 'yosay'
import chalk from 'chalk'
import path from 'path'
import fs from 'fs'

export default class QuarkusKotlinGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts)
    this.argument('moduleName', {type: String, required: false})
  }

  prompting() {
    this.log(yosay(
      'Welcome to the first ' + chalk.red('generator-quarkus-hexagonal:module') + ' generator!'
    ))

    const prompts = [{
      type: 'input',
      name: 'moduleName',
      message: 'Your new module name',
      default: this.options.moduleName
    }]

    return this.prompt(prompts).then(props => {
      this.props = props
    })
  }
  default() {
    console.log('config', this.config)
    console.log('props', this.props)    
  }

  writing() {
    // this.config.set(this.props)
    // this.config.save()
	// const pkg = this.props.groupId.replace(/[.]/g, '/')
	// const javaMainSrc = `src/main/java/${pkg}`
	// const javaTestSrc = `src/test/java/${pkg}`
    const groupId = this.config.get('groupId')
    const projectName = this.config.get('projectName')
    console.log('reloaded', groupId, name)

    this.fs.copyTpl(this.templatePath('MODULE.md'), this.destinationPath(`MODULE-${this.props.moduleName}.md`), { ...this.props, projectName, groupId })
    // this.fs.copyTpl(this.templatePath('settings.gradle'), this.destinationPath('settings.gradle'), this.props)
    // this.fs.copyTpl(this.templatePath('Hello.java'), this.destinationPath(`${javaMainSrc}/hello/Hello.java`), this.props)
    // this.fs.copyTpl(this.templatePath('HelloTest.java'), this.destinationPath(`${javaTestSrc}/hello/HelloTest.java`), this.props)
  }
}
